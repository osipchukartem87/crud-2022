<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    public function getAllSum(): float
    {
        $sum = [];
        foreach ($this['order_items'] as $item) {
            $sum[] = ($item->price / 100 * $item->quantity) / 100 * (100 - $item->discount);
        }

        return sprintf("%01.2f", array_sum($sum));
    }

    public function getAllOrderItems(): array
    {
        $result = [];
        foreach ($this['order_items'] as $item) {
            $result[] = [
                'productName' => $item->product,
                'productQty' => $item->quantity,
                'productPrice' => $item->price / 100,
                'productDiscount' => $item->discount,
                'productSum' => sprintf("%01.2f", ($item->price / 100 * $item->quantity) / 100 * (100 - $item->discount)),
            ];
        }

        return $result;
    }

    public function toArray($request): array
    {

        return [
            'orderId' => $this['order']->id,
            'orderDate' => $this['order']->date,
            'orderSum' => $this->getAllSum(),
            'orderItems' => $this->getAllOrderItems(),
            'buyer' => $this['buyer']->id,
            'buyerFullName' => $this['buyer']->name . ' ' . $this['buyer']->surname,
            'buyerAddress' => $this['buyer']->country . ', ' . $this['buyer']->city . ', ' . $this['buyer']->addressLine,
            'buyerPhone' => $this['buyer']->phone,
        ];
    }
}
