<?php

namespace App\Http\Controllers;

use App\Http\Resources\OrderResource;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Models\Order;
use Illuminate\Http\Response;

class OrderController extends Controller
{
    public function index(): Collection
    {
        return Order::all();
    }

    public function store(Request $request)
    {

        $data = $request->all();

        $order = Order::all();

        $order->buyer->id = $data['buyerId'];

        foreach ($data['orderItems'] as $key => $value) {
            $order->orderItems[$key]->product = $value['product'];
            $order->orderItems[$key]->quantity = $value['quantity'];
            $order->orderItems[$key]->discount = $value['discount'];
        }
        $order->save();

        return new Response($order);
    }

    public function show(int $id)
    {
        $data['order'] = Order::findOrFail($id);
        $data['buyer'] = Order::findOrFail($id)->buyer;
        $data['order_items'] = Order::findOrFail($id)->orderItems;

        return new OrderResource($data);
    }

    public function update(Request $request, int $id)
    {
        $order = Order::findOrFail($id);

        if (!$order) {
            return new Response([
                'result' => 'fail',
                'message' => 'order not found'
            ]);
        }

        $data = $request->all();

        $order->id = $data['orderId'];

        foreach ($data['orderItems'] as $key => $value) {
            $order->orderItems[$key]->product = $value['product'];
            $order->orderItems[$key]->quantity = $value['quantity'];
            $order->orderItems[$key]->discount = $value['discount'];
        }
        $order->save();

        return new Response($order);
    }

    public function destroy(int $id): array
    {
        $result = Order::destroy($id);
        return ['result' => $result ? 'success' : 'fail'];
    }
}
