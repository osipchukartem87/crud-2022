<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class BuyerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->firstName,
            'surname' => $this->faker->lastName,
            'country' => $this->faker->country,
            'city' => $this->faker->numberBetween(1, 100),
            'addressLine' => $this->faker->address,
            'phone' => $this->faker->phoneNumber,
        ];
    }
}
