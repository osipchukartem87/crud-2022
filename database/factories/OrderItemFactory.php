<?php

namespace Database\Factories;

use App\Models\OrderItem;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'product' => $this->faker->numberBetween(1, 20),
            'quantity' => $this->faker->numberBetween(1, 10),
            'price' => $this->faker->numberBetween(500, 50000),
            'discount' => $this->faker->numberBetween(1, 100),
            'sum' => OrderItem::getSum(),
        ];
    }
}
