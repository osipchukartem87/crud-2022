<?php

namespace Database\Seeders;

use Database\Factories\BuyerFactory;
use Database\Factories\OrderFactory;
use Database\Factories\OrderItemFactory;
use Illuminate\Database\Seeder;

class BuyerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        (new BuyerFactory(10))->create()
            ->each(function ($buyer) {
                $buyer->orders()->saveMany(
                    (new OrderFactory(10))->create()
                        ->each(function ($order) {
                            $order->orderItems()->saveMany(
                                (new OrderItemFactory(10))->create()
                            );
                        })
                );
            });
    }
}
